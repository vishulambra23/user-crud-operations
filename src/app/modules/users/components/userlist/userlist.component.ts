import { Component, OnInit, ViewChild, ElementRef, AfterViewInit, ChangeDetectionStrategy} from '@angular/core';
import { IUser } from '../../../../models/users';
import { Store, select } from '@ngrx/store';
import * as UserActions from '../../../../user.actions';
import * as fromUser from '../../../../user.selectors';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms'
import { ModalType } from 'src/constants/constants';

@Component({
  selector: 'app-userlist',
  templateUrl: './userlist.component.html',
  styleUrls: ['./userlist.component.css']
})
export class UserlistComponent implements OnInit {

  pageTitle = 'Users List';
  errorMessage = '';
  users: IUser[];
  userFilter;
  userForm: FormGroup;
  modalType:ModalType = ModalType.XLLARGE;
  openUserPopUp:boolean = false;
  editUser: any = {};
  selectedUserId: string;

  constructor(private store: Store) { }

  ngOnInit(): void {

    // if(this){
    //   this.store.dispatch(new UserActions.LoadUsers()); // action dispatch
    // }
    // this.initUserForm();
    // this.addNewFriend();
    this.store.pipe(select(fromUser.getUsers)).subscribe(
      users => {
        this.users = users;
      }
    )

    this.store.pipe(select(fromUser.getError)).subscribe(
      err => {
        this.errorMessage = err;
      }
    )
  }
  

  deleteUser(){
    this.store.dispatch(new UserActions.UserDeleteAction({id:this.selectedUserId}));
    this.closePopup();
    this.selectedUserId = '';
  }

  openPopUp(id:string){
    this.selectedUserId = id;
    this.openUserPopUp = true;
  }
  closePopup(){
    this.openUserPopUp = false;
  }

}
