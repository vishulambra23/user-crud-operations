import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Store, select } from '@ngrx/store';
import { IUser } from '../../../../models/users';
import * as UserActions from '../../../../user.actions';
import * as fromUser from '../../../../user.selectors';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-adduser',
  templateUrl: './adduser.component.html',
  styleUrls: ['./adduser.component.css']
})
export class AdduserComponent implements OnInit {
  userForm: FormGroup;
  constructor(private fb:FormBuilder,private store: Store,private router: Router,private service: UserService) { }

  ngOnInit(): void {
    this.initUserForm();
  }

  initUserForm(){
    this.userForm = this.fb.group({
      name:['',[Validators.required]],
      email:['',[Validators.required,Validators.email]],
      phone:['',[Validators.required,Validators.minLength(10)]],
      company:['',[Validators.required]],
      about:['',[Validators.required]],
      age:['',[Validators.required]],
      gender:['',[Validators.required]],
      friendsArray: this.fb.array([])
    });
    this.addNewFriend()
  }

  addNewFriend(){
    const userFriends = this.userForm.get('friendsArray') as FormArray;
    userFriends.push(this.createNewFriend());
  }

  createNewFriend(){
    return this.fb.group({
      name:['',[Validators.required]]
    })
  }

  addUser(){
    if(this.userForm.invalid){
      this.service.validateAllFormFields(this.userForm);
      return;
    }
    let user:any = {
        '_id': Math.random(),
        "guid": "ca8162f1-4c97-447d-82d0-f651243ec7bd",
        "isActive": false,
        "balance": "$1,348.34",
        "picture": "http://placehold.it/32x32",
        "eyeColor": "brown",
        "address": "330 Preston Court, Camptown, Virgin Islands, 4109",
        "registered": "2014-09-08T02:00:38 -06:-30",
        "latitude": -49.428141,
        "longitude": -111.509166,
        "tags": ["velit", "nulla", "cillum", "officia", "est", "sunt", "nostrud"],
        "greeting": "Hello, Dorothy Holland! You have 5 unread messages.",
        "favoriteFruit": "banana"
    };
    user['name'] = this.userForm.controls.name.value;
    user['email'] = this.userForm.controls.email.value;
    user['phone'] = this.userForm.controls.phone.value;
    user['age'] = this.userForm.controls.age.value;
    user['company'] = this.userForm.controls.company.value;
    user['about'] = this.userForm.controls.about.value;
    user['friends'] = this.userForm.controls.friendsArray.value
    this.store.dispatch(new UserActions.UserAddAction(user));
    this.router.navigate(['/list'])
  }

  deleteFriend(index){
    const userFriend = this.userForm.get('friendsArray') as FormArray;
    userFriend.removeAt(index);
  }

}
