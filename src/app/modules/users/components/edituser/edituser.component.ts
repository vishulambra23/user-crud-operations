import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Store, select } from '@ngrx/store';
import { IUser } from '../../../../models/users';
import * as UserActions from '../../../../user.actions';
import * as fromUser from '../../../../user.selectors';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-edituser',
  templateUrl: './edituser.component.html',
  styleUrls: ['./edituser.component.css']
})
export class EdituserComponent implements OnInit {

  constructor(private store:Store,public fb:FormBuilder,private route: ActivatedRoute,private router: Router,private service: UserService) { }
  user: IUser[];
  userId: string;
  userForm: FormGroup
  ngOnInit(): void {
    this.route.params.subscribe((data)=>{
      if(data.id){
        this.userId = data.id;
      }
    })
    this.store.pipe(select(fromUser.getUsers)).subscribe(
      users => {
        this.user = users.filter((user)=>{ return user._id === this.userId});
        if(this.user){
          this.initUserForm(this.user[0]);
        }
      }
    )
  }


  initUserForm(user){
    this.userForm = this.fb.group({
      name:[ (user && user.name) ? user.name : '',[Validators.required]],
      email:[ (user && user.email) ? user.email : '',[Validators.required,Validators.email]],
      phone:[ (user && user.phone) ? user.phone : '',[Validators.required,Validators.minLength(10)]],
      company:[ (user && user.company) ? user.company : '',[Validators.required]],
      about:[ (user && user.about) ? user.about : '',[Validators.required]],
      age:[ (user && user.age) ? user.age : '',[Validators.required]],
      gender:[ (user && user.gender) ? user.gender :'' ,[Validators.required]],
      friendsArray: this.fb.array([])
    });
    if(user && user.friends){
      user.friends.forEach(element => {
        this.addNewFriend(element)
      });
    }else
    this.addNewFriend()
  }
  
  addNewFriend(friend?){
    const userFriends = this.userForm.get('friendsArray') as FormArray;
    userFriends.push(this.createNewFriend(friend));
  }

  createNewFriend(friend){
    return this.fb.group({
      name:[(friend && friend.name) ? friend.name : '',[Validators.required]]
    })
  }
  
  setDefaultValues(userData){
    console.log(userData);
    this.userForm.patchValue(userData[0]);
  }
  updateUser(){
    if(this.userForm.invalid){
      this.service.validateAllFormFields(this.userForm);
      return;
    }
    let user:any = {
      ...this.user[0]
    };
    user['name'] = this.userForm.controls.name.value;
    user['email'] = this.userForm.controls.email.value;
    user['phone'] = this.userForm.controls.phone.value;
    user['age'] = this.userForm.controls.age.value;
    user['company'] = this.userForm.controls.company.value;
    user['about'] = this.userForm.controls.about.value;
    this.store.dispatch(new UserActions.UserUpdateAction(user));
    this.router.navigate(['/list']);
  }

  deleteFriend(index){
    const userFriend = this.userForm.get('friendsArray') as FormArray;
    userFriend.removeAt(index);
  }

}
