import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormArray, FormControl, FormGroup } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  // private userUrl = 'assets/mock.json';

  constructor(private http: HttpClient) { }

  // getUsers(): Observable<IUser[]> {
  //   return this.http.get<IUser[]>(this.userUrl)
  //     .pipe(
  //       catchError(this.handleError)
  //     );
  // }
    /**
   * to fire all errors before submission
   * @param form FormGroup to be validated.
   */
     public validateAllFormFields(form: FormGroup) {
      const keys = Object.keys(form.controls);
      keys.forEach((field: any) => {
        const control = form.get(field);
        if (control instanceof FormControl) {
          control.markAsTouched({ onlySelf: true });
          control.markAsDirty({ onlySelf: true });
        } else if (control instanceof FormGroup) {
          this.validateAllFormFields(control);
        } else if (control instanceof FormArray) {
          (<FormArray>control).controls.forEach((element: FormGroup) => {
            this.validateAllFormFields(element);
          });
        }
      });
    }

  // deleteUser(Id: string): Observable<any> {
  //   return this.http.delete(this.userUrl + Id)
  //   .pipe(
  //     catchError(this.handleError)
  //   );
  // }

  // private handleError(err: HttpErrorResponse) {
  //   // in a real world app, we may send the server to some remote logging infrastructure
  //   // instead of just logging it to the console
  //   let errorMessage = '';
  //   if (err.error instanceof ErrorEvent) {
  //     // A client-side or network error occurred. Handle it accordingly.
  //     errorMessage = `An error occurred: ${err.error.message}`;
  //   } else {
  //     // The backend returned an unsuccessful response code.
  //     // The response body may contain clues as to what went wrong,
  //     errorMessage = `Server returned code: ${err.status}, error message is: ${err.message}`;
  //   }
  //   console.error(errorMessage);
  //   return throwError(errorMessage);
  // }
}
