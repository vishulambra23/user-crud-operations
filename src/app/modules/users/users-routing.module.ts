import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdduserComponent } from './components/adduser/adduser.component';
import { EdituserComponent } from './components/edituser/edituser.component';
import { UserlistComponent } from './components/userlist/userlist.component';

const routes: Routes = [
  {
    path: 'list',
    component: UserlistComponent
  },{
    path: 'add',
    component: AdduserComponent
  },{
    path: 'edit/:id',
    component: EdituserComponent
  },
  {
    path: '',
    redirectTo: 'list'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule { }
