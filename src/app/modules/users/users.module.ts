import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsersRoutingModule } from './users-routing.module';
import { UserlistComponent } from './components/userlist/userlist.component';
import { AdduserComponent } from './components/adduser/adduser.component';
import { EdituserComponent } from './components/edituser/edituser.component';
import { ReactiveFormsModule } from '@angular/forms';
import { PopupModule } from '../popup/popup.module';


@NgModule({
  declarations: [
    UserlistComponent,
    AdduserComponent,
    EdituserComponent
  ],
  imports: [
    CommonModule,
    UsersRoutingModule,
    ReactiveFormsModule,
    PopupModule
  ]
})
export class UsersModule { }
