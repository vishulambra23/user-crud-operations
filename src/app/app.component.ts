import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import * as UserActions from './user.actions';
import * as fromUser from './user.selectors';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  constructor(private store: Store){}
  title = 'users-list';

  ngOnInit(){
    this.store.dispatch(new UserActions.LoadUsers()); // action dispatch
  }
}
