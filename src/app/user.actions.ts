import { Action } from '@ngrx/store';
import { IUser } from './models/users';
export enum UserActionTypes {
  LoadUsers = '[User] Load Users',
  LoadUsersSuccess = '[User] Load Users Success',
  LoadUsersFailure = '[User] Load Users Failure',
  UserDeleteAction = '[User] Delete',
  UserUpdateAction = '[User] Update User Data',
  UserAddAction = '[User] Add New User'
}

export class LoadUsers implements Action {
  readonly type = UserActionTypes.LoadUsers;
}

export class LoadUsersSuccess implements Action {
  readonly type = UserActionTypes.LoadUsersSuccess;
  constructor(public payload: { data: IUser[] }) { }
}

export class LoadUsersFailure implements Action {
  readonly type = UserActionTypes.LoadUsersFailure;
  constructor(public payload: { error: string }) { }
}

export class UserDeleteAction implements Action {
  readonly type = UserActionTypes.UserDeleteAction;
  constructor(public payload: { id: string }) { }
}

export class UserUpdateAction implements Action {
  readonly type = UserActionTypes.UserUpdateAction;
  constructor(public payload: { data: any }) { }
}

export class UserAddAction implements Action {
  readonly type = UserActionTypes.UserAddAction;
  constructor(public payload: { data: any }) { }
}

export type UserActions = LoadUsers | LoadUsersSuccess | LoadUsersFailure | UserDeleteAction | UserUpdateAction | UserAddAction;

