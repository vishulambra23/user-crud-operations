import { Action } from '@ngrx/store';
import { IUser } from './models/users';
import { UserActions, UserActionTypes } from './user.actions';


export const userFeatureKey = 'usersState';

export interface State {
  users: IUser[],
  error: string
}

export const initialState: State = {
  users: [],
  error: ''
};

export function reducer(state = initialState, action: any): State {
  switch (action.type) {

    case UserActionTypes.LoadUsers:
      return {
        ...state
      }
    case UserActionTypes.LoadUsersSuccess:
      return {
        ...state,
        users: action.payload.data,
        error: ''
      }
    case UserActionTypes.UserDeleteAction:{
        const users = state.users.filter(data=>data._id !== action.payload.id);
        return {
          ...state,
          ...{users},
          error: ''
        }
      }
    case UserActionTypes.UserUpdateAction: {
      const users = state.users.filter(data=>data._id !== action.payload._id);
      users.splice(action.payload.index,0,action.payload);
      return {
        ...state,
        ...{users},
        error: ''
      }
    }
    case UserActionTypes.UserAddAction:{
      const users = state.users.concat(action.payload);
      return {
        ...state,
        ...{users},
        error: ''
      }
    }
    case UserActionTypes.LoadUsersFailure:
      return {
        ...state,
        users: [],
        error: action.payload.error
      }
    default:
      return state;
  }
}
