export interface IUser { 
    "_id": string,
    "index": number,
    "guid": string,
    "isActive": boolean,
    "balance": string,
    "picture": string,
    "age": number,
    "eyeColor": string,
    "name": string,
    "gender": string,
    "company": string,
    "email": string,
    "phone": string,
    "address": string,
    "about": string,
    "latitude": number,
    "longitude": number,
    "tags":Array<string>,
    "greeting": string,
    "favoriteFruit": string,
    "friends":Array<IFriends>,
    "registered":string;
}

export interface IFriends{
    "id": number,
    "name": string
}