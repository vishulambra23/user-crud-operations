import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable,of } from 'rxjs';
import {Action } from "@ngrx/store";
import * as UserActions from './user.actions';
import { mergeMap, map, catchError } from 'rxjs/operators';
import { UsersService } from './services/users.service';

@Injectable()
export class UserEffects {



  constructor(private actions$: Actions, private userService: UsersService) {}

  @Effect()
  loadUsers$: Observable<Action> = this.actions$.pipe(ofType(UserActions.UserActionTypes.LoadUsers),
  mergeMap( action => this.userService.getUsers().pipe(
    map(users => (new UserActions.LoadUsersSuccess({data: users}))),
    catchError(err => of(new UserActions.LoadUsersFailure({error: err})))
  )))

  // UserDeleteAction$: Observable<Action> = this.actions$.pipe(ofType(UserActions.UserActionTypes.UserDeleteAction),
  // mergeMap( action => this.userService.deleteUser(payload.id).pipe(
  //   map(users => (new UserActions.UserDeleteAction({data: users.}))),
  //   catchError(err => of(new UserActions.LoadUsersFailure({error: err})))
  // )))

}
