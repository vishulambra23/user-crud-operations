import { createFeatureSelector, createSelector } from '@ngrx/store';
import { State } from "./user.reducer";

const getUsersFeatureState = createFeatureSelector<State>('usersState');

export const getUsers = createSelector(
    getUsersFeatureState,
    state => state.users
)

export const getError = createSelector(
    getUsersFeatureState,
    state => state.error
)